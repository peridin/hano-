#!/usr/bin/env python3

import os
import time
import pprint as pp

nb_disks = 8
hanoi = { 1 : [], 2 : [], 3 : []}
for d in range(nb_disks):
    hanoi[1].insert(0, d+1)
hanoi_logs = open('hanoi.log', 'w')

clear = lambda: os.system('clear')

def show_hanoi(hanoi, infos = ''):
    clear()
    screen = []
    if infos != '':
        screen.append(infos)
    line = ''
    for i in [1, 2, 3]:
        line += ( "taille = " + str(len(hanoi[i]))).center(2 * nb_disks) + ' '
    screen.append(line)
    screen.append(('-' * 2 * nb_disks + ' ') * 3)
    for disk in range(nb_disks + 1):
        if disk  >=  nb_disks:
            print()
        else:
            line = ''
            for i in [1, 2, 3]:
                if disk < len(hanoi[i]):
                    #line += (str(hanoi[i][disk]) * 2 * hanoi[i][disk]).center(2 * nb_disks) + ' '
                    line += ' ' * (nb_disks - hanoi[i][disk]) + '\x1b[6;30;' + str(40 + hanoi[i][disk] % 6) + 'm' + ' ' * 2 * hanoi[i][disk] + '\x1b[0m' + ' ' * (nb_disks - hanoi[i][disk]) + ' '
                else:
                    line += ' ' * 2 * nb_disks + ' '
            screen.append(line)
    while len(screen) > 0:
        print(screen.pop())
    time.sleep(0.5)

def resolve_hanoi(hanoi, index, start, target):
    size = len(hanoi[start]) - index    # taille du groupe de disques à déplacer
    inter = 6 - start - target
    print('start = ' + str(start) + ', target = ' + str(target) + ', size = ' + str(size), file=hanoi_logs)
    show_hanoi(hanoi)
    # Premier mouvement
    if size > 4:
        resolve_hanoi(hanoi, len(hanoi[start]) - size + 2, start, target)
    elif size == 4:
        resolve_hanoi(hanoi, len(hanoi[start]) - size + 1, start, inter)
        element = hanoi[start].pop()
        hanoi[target].append(element)
        show_hanoi(hanoi)
        resolve_hanoi(hanoi, len(hanoi[inter]) - size + 1, inter, target)
        return
    else:
        element = hanoi[start].pop()
        hanoi[target].append(element)
        show_hanoi(hanoi)
    # Deuxième mouvement
    element = hanoi[start].pop()
    hanoi[inter].append(element)
    show_hanoi(hanoi)
    # Troisieme mouvement
    if size > 3:
        resolve_hanoi(hanoi, len(hanoi[target]) - size + 2, target, inter)
    else:
        element = hanoi[target].pop()
        hanoi[inter].append(element)
        show_hanoi(hanoi)
    # Quatrieme mouvement
    element = hanoi[start].pop()
    hanoi[target].append(element)
    show_hanoi(hanoi)
    # Cinquieme mouvement
    if size > 3:
        resolve_hanoi(hanoi, len(hanoi[inter]) - size + 2, inter, start)
    else:
        element = hanoi[inter].pop()
        hanoi[start].append(element)
        show_hanoi(hanoi)
    # Sixième mouvement
    element = hanoi[inter].pop()
    hanoi[target].append(element)
    show_hanoi(hanoi)
    # Septième mouvement
    if size > 3:
        resolve_hanoi(hanoi, len(hanoi[start]) - size + 2, start, target)
    else:
        element = hanoi[start].pop()
        hanoi[target].append(element)
        show_hanoi(hanoi)


if __name__ == '__main__':
    resolve_hanoi(hanoi, index = 0, start = 1, target = 3)